import sys
sys.path.append("/src/app")
from config import Config

import datetime
import pytz
import json
import uuid
import dateutil
from pbglobal.commands.sendTaskReminder import sendTaskReminder

# Compensate for processing-time in seconds. 10 Means, reminder will be triggered 10 seconds earlier than set
compensation_value = 10

sql = "SELECT * FROM task_reminders as er inner join queue_entries AS e on e.entry_id = er.task_id where processed_time is null"
results = Config.db_session.execute(sql).fetchall()
current_timezone = pytz.timezone("Europe/Berlin")

for row in results:
    task_data = json.loads(row.data)
    task_time = task_data.get("due_date")
    if task_time is None:
        continue

    current_time = datetime.datetime.now(pytz.timezone('Europe/Berlin'))
    command_creation_time = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

    localized_starttime = dateutil.parser.isoparse(task_time)
    seconds_until_task = localized_starttime.timestamp() - current_time.timestamp()

    # Only do something if event hasn't passed already
    if True or seconds_until_task >= -600:
        reminder_definition = json.loads(row.definition)
        if reminder_definition.get("seconds_in_advance") + compensation_value >= seconds_until_task:
            if reminder_definition.get("reminder_type") == "gui":
                sent_command = sendTaskReminder(reminder_id=row.reminder_id, owner=row.owner, creator=row.creator, correlation_id=str(uuid.uuid4()), create_time=command_creation_time)
                sent_command.publish()
                sql = f"UPDATE task_reminders SET processed_time = now() where processed_time is null and reminder_id = :reminder_id"
                results = Config.db_session.execute(sql, {"reminder_id": row.reminder_id})
                Config.db_session.flush()
            elif reminder_definition.get("reminder_type") == "email":
                sent_command = sendTaskReminder(reminder_id=row.reminder_id, owner=row.owner, creator=row.creator, correlation_id=str(uuid.uuid4()), create_time=command_creation_time)
                sent_command.publish()
                sql = f"UPDATE task_reminders SET processed_time = now() where processed_time is null and reminder_id = :reminder_id"
                results = Config.db_session.execute(sql, {"reminder_id": row.reminder_id})
                Config.db_session.flush()


