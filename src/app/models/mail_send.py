import os
import os.path
import datetime
from M2Crypto import BIO, Rand, SMIME
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders
import boto3
import smtplib, ssl

from email.message import Message
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

from jinja2 import Environment, FileSystemLoader
import requests
import dateutil

# TODO MOUNT CERTS INTO CONTAINER INSTEAD OF NEEDING THEM IN THE REPOSITORY
signMail = True
if os.getenv("MAIL_CERTIFICATES_KEY_PATH") is not None and os.path.isfile(os.getenv("MAIL_CERTIFICATES_KEY_PATH")):
    ssl_key = os.getenv("MAIL_CERTIFICATES_KEY_PATH")
else:
    signMail = False
if os.getenv("MAIL_CERTIFICATES_CRT_PATH") is not None and os.path.isfile(os.getenv("MAIL_CERTIFICATES_CRT_PATH")):
    ssl_cert = os.getenv("MAIL_CERTIFICATES_CRT_PATH")
else:
    signMail = False


sender = 'no-reply@serviceblick.com'
nextweekreminder_mail = '/src/app/ressources/nextweekreminder_mail.txt'

file_loader = FileSystemLoader('/src/app/ressources')
env = Environment(loader=file_loader)

def get_account_signature(account_id):
    url = os.getenv("ACCOUNTMANAGER_STORE_BASE_URL") + f"/get_account_profile?account_id={account_id}"

    response = requests.request("GET", url)

    signature = response.json().get("email_signature")
    return signature


def utc_str_to_local_str(utc_str: str, utc_format: str, local_format: str):
    """
    :param utc_str: UTC time string
    :param utc_format: format of UTC time string
    :param local_format: format of local time string
    :return: local time string
    """
    temp1 = datetime.datetime.strptime(utc_str, utc_format)
    temp2 = temp1.replace(tzinfo=datetime.timezone.utc)
    local_time = temp2.astimezone()
    return local_time.strftime(local_format)

def send_single_task_reminder_mail(recipient, task_payload):
    subject = 'Erinnerung an eine Aufgabe'
    template_text = env.get_template('singletaskreminder_mail.txt')
    template_html = env.get_template('singletaskreminder_mail.html')
    if len(recipient) < 5:
        print(f'Cannot send mail to recipient "{recipient}"')
        return True
    try:
        to = [recipient]
        login = os.getenv("SMTP_USERNAME")
        password = os.getenv("SMTP_PASSWORD")

        signature = get_account_signature(task_payload.get("owner")) or ""

        due_date = dateutil.parser.isoparse(task_payload.get("due_date"))
        from_zone = dateutil.tz.tzutc()
        to_zone = dateutil.tz.gettz('Europe/Berlin')

        due_date = due_date.replace(tzinfo=from_zone)

        # Convert time zone
        due_date = due_date.astimezone(to_zone).strftime("%d.%m.%Y, %H:%M")

        body = template_text.render(due_date=due_date, task=task_payload, signature=signature)
        body_html = template_html.render(due_date=due_date, task=task_payload, signature=signature)
        msg = MIMEMultipart('alternative')

        basemsg = MIMEText(body, 'plain')
        msg.attach(basemsg)

        basemsg_html = MIMEText(body_html, 'html')
        msg.attach(basemsg_html)



        if signMail:
            msg_str = msg.as_string()
            buf = BIO.MemoryBuffer(msg_str.encode())

            # load seed file for PRNG
            Rand.load_file('/tmp/randpool.dat', -1)
            smime = SMIME.SMIME()

            # load certificate
            smime.load_key(ssl_key, ssl_cert)

            # sign whole message
            p7 = smime.sign(buf, SMIME.PKCS7_DETACHED)

            # create buffer for final mail and write header
            out = BIO.MemoryBuffer()
            out.write('From: %s\n' % sender)
            out.write('To: %s\n' % COMMASPACE.join(to))
            out.write('Date: %s\n' % formatdate(localtime=True))
            out.write('Subject: %s\n' % subject)
            out.write('Auto-Submitted: %s\n' % 'auto-generated')

            # convert message back into string
            buf = BIO.MemoryBuffer(msg_str.encode())

            # append signed message and original message to mail header
            smime.write(out, p7, buf)

            # load save seed file for PRNG
            Rand.save_file('/tmp/randpool.dat')

            content = out.read()
        else:
            msg['Subject'] = subject
            msg['From'] = sender
            msg['To'] = COMMASPACE.join(to)
            content = msg.as_string()


        # Log in to server using secure context and send email
        if os.getenv("SMTP_SSL") == "true":
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT"), context=context) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()
        else:
            with smtplib.SMTP(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT")) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()


        print('send_single_event_reminder_mail mail sent')
        return True
    except Exception as e:
        print(e)
        import traceback
        print(traceback.format_exc())
        return False


