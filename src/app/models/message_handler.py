import os
from pipes import Template
from re import template
import requests
import json
from time import sleep
from pbglobal.pblib.amqp import client
from config import Config
import traceback
import uuid
from datetime import datetime, timedelta
import dateutil
import pytz

from pbglobal.events.newQueueCreated import newQueueCreated
from pbglobal.events.queueUpdated import queueUpdated
from pbglobal.events.queueDeleted import queueDeleted
from pbglobal.events.newQueueEntryCreated import newQueueEntryCreated
from pbglobal.events.queueEntryUpdated import queueEntryUpdated
from pbglobal.events.queueEntryDeleted import queueEntryDeleted
from pbglobal.events.queueDataOwnerAdded import queueDataOwnerAdded
from pbglobal.events.queueDataOwnerRemoved import queueDataOwnerRemoved
from pbglobal.events.queueEntryDataOwnerAdded import queueEntryDataOwnerAdded
from pbglobal.events.queueEntryDataOwnerRemoved import queueEntryDataOwnerRemoved
from pbglobal.events.queueEntriesOrderUpdated import queueEntriesOrderUpdated
from pbglobal.events.newTaskTemplateCreated import newTaskTemplateCreated
from pbglobal.events.taskTemplateDeleted import taskTemplateDeleted
from pbglobal.events.newTaskManagerResourceCreated import newTaskManagerResourceCreated
from pbglobal.events.taskManagerResourceUpdated import taskManagerResourceUpdated
from pbglobal.events.taskManagerResourceDeleted import taskManagerResourceDeleted
from pbglobal.commands.setTaskReminder import setTaskReminder
from pbglobal.events.taskReminderSet import taskReminderSet
from pbglobal.commands.deleteTaskReminder import deleteTaskReminder
from pbglobal.events.taskReminderDeleted import taskReminderDeleted
from pbglobal.commands.sendTaskReminder import sendTaskReminder
from pbglobal.events.taskReminderSent import taskReminderSent
from pbglobal.events.newTaskManagerTemplateGroupCreated import newTaskManagerTemplateGroupCreated
from pbglobal.events.taskManagerTemplateGroupUpdated import taskManagerTemplateGroupUpdated
from pbglobal.events.taskManagerTemplateGroupDeleted import taskManagerTemplateGroupDeleted

from pbglobal.events.newAppointmentCreated import newAppointmentCreated
from pbglobal.events.appointmentDeleted import appointmentDeleted

from db.queue import Queue
from db.queue_entry import QueueEntry
from db.task_template import TaskTemplate
from db.task_reminder import TaskReminder
from db.resource import Resource
from db.template_group import TemplateGroup
from models.task_reminders import Reminder

from pbglobal.pblib.api_calls import ApiCalls


class message_handler():
    kong_api = "http://kong.planblick.svc:8001"
    queue_client = client()

    def handle_message(ch, method=None, properties=None, body=None):
        try:
            print("---------- Start handling Message ----------")
            event = method.routing_key
            body = body.decode("utf-8")
            instance = message_handler()
            if (event is not None):
                handler_exists = method.routing_key in dir(instance)

                if handler_exists is True:
                    if getattr(instance, method.routing_key)(ch, body) == True:
                        ch.basic_ack(method.delivery_tag)
                    else:
                        ch.basic_nack(method.delivery_tag)
                else:
                    raise Exception("No handler for this  message or even not responsible: '" + event + "'")

        except Exception as e:
            print(e)
            traceback.print_exc()
            ch.basic_nack(method.delivery_tag)
            sleep(15)

        print("---------- End handling Message ----------")

    def updateQueueEntriesOrder(self, ch, msg):
        try:
            print("Handling updateQueueEntriesOrder")
            command_payload = json.loads(msg)
            event = queueEntriesOrderUpdated.from_json(queueEntriesOrderUpdated, command_payload)
            event.publish()
        except Exception as e:
            print(e)
            traceback.print_exc()
            sleep(3)
            return False

        return True

    def queueEntriesOrderUpdated(self, ch, msg):
        try:
            print("Handling queueEntriesOrderUpdated")
            payload = json.loads(msg)
            entries_order = payload.get("entries_order", {})
            owner = payload.get("owner")
            queue_entry_id = None
            queue_queue_id = None
            for (entry_id, order) in entries_order.items():
                queue_entry = Config.db_session.query(QueueEntry) \
                    .filter(QueueEntry.consumer_id == owner) \
                    .filter(QueueEntry.entry_id == entry_id) \
                    .first()

                data = json.loads(queue_entry.data)
                data["order"] = order
                queue_entry.data = json.dumps(data)

                queue_entry.order = order
                queue_entry_id = queue_entry.entry_id
                queue_queue_id = queue_entry.queue_id
                queue_entry.save()

            message = {"command": "refreshQueueEntries",
                       "args": {"queue_id": queue_queue_id, "queue_entry_id": queue_entry_id,
                                "task_id": payload.get("correlation_id")}}

            payload = {"room": owner, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            sleep(3)
            return False

        return True

    def createNewQueue(self, ch, msg):
        print("Handling createNewQueue")
        command_payload = json.loads(msg)
        event = newQueueCreated.from_json(newQueueCreated, command_payload)
        event.publish()
        return True

    def newQueueCreated(self, ch, msg):
        try:
            print("Handling newQueueCreated")
            json_data = json.loads(msg)
            new_queue = newQueueCreated.from_json(newQueueCreated, json_data)

            Config.db_session.query(Queue) \
                .filter(Queue.consumer_id == new_queue.owner) \
                .filter(Queue.queue_id == new_queue.queue_id) \
                .delete()

            queue = Queue()
            queue.consumer_id = new_queue.owner
            queue.creator = new_queue.creator
            queue.owner_id = new_queue.owner
            queue.queue_id = new_queue.queue_id
            queue.data = json.dumps(new_queue.queue_data)
            queue.data_owners = json.dumps(new_queue.data_owners) if new_queue.data_owners is not None else json.dumps(
                [f"user.{queue.creator}"])
            queue.save()

            message = {"command": "refreshQueues",
                       "args": {"queue_id": queue.queue_id, "task_id": json_data.get("correlation_id")}}

            payload = {"room": queue.consumer_id, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def updateQueue(self, ch, msg):
        print("Handling updateQueue")
        command_payload = json.loads(msg)
        event = queueUpdated.from_json(queueUpdated, command_payload)
        event.publish()
        return True

    def queueUpdated(self, ch, msg):
        try:
            print("Handling queueUpdated")
            json_data = json.loads(msg)
            queue_updated_event = queueUpdated.from_json(queueUpdated, json_data)

            queue = Config.db_session.query(Queue) \
                .filter(Queue.consumer_id == queue_updated_event.owner) \
                .filter(Queue.queue_id == queue_updated_event.queue_id) \
                .first()

            if not queue:
                raise Exception("Queue not found")

            requesters_group_memberships = ApiCalls().getGroupMembershipsByCustomerIdAndLogin(json_data.get("owner"),
                                                                                              json_data.get("creator"))

            data_owners = json.loads(queue.data_owners)
            matches = [match for match in requesters_group_memberships if match in data_owners]
            if f"user.{queue_updated_event.creator}" in data_owners:
                matches.append(f"user.{queue_updated_event.creator}")

            if len(matches) == 0 and len(data_owners) > 0:
                message = {"command": "showTaskError",
                           "args": {"text": "You are not allowed to edit this queue",
                                    "task_id": json_data.get("correlation_id")}}
                payload = {"room": queue.consumer_id, "data": message}
                client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
                return True

            queue.creator = queue_updated_event.creator
            queue.data = json.dumps(queue_updated_event.queue_data)

            if queue_updated_event.data_owners:
                queue.data_owners = json.dumps(queue_updated_event.data_owners)

            queue.save()

            message = {"command": "refreshQueues",
                       "args": {"queue_id": queue.queue_id, "task_id": json_data.get("correlation_id")}}

            payload = {"room": queue.consumer_id, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def deleteQueue(self, ch, msg):
        print("Handling deleteQueue")
        command_payload = json.loads(msg)
        event = queueDeleted.from_json(queueDeleted, command_payload)
        event.publish()
        return True

    def queueDeleted(self, ch, msg):
        try:
            print("Handling queueDeleted")
            json_data = json.loads(msg)
            queue_deleted_event = queueDeleted.from_json(queueDeleted, json_data)
            Config.db_session.query(Queue) \
                .filter(Queue.consumer_id == queue_deleted_event.owner) \
                .filter(Queue.queue_id == queue_deleted_event.queue_id) \
                .delete()

            message = {"command": "refreshQueues",
                       "args": {"queue_id": "deleted", "task_id": json_data.get("correlation_id")}}

            payload = {"room": queue_deleted_event.owner, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))

        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def createNewQueueEntry(self, ch, msg):
        print("Handling createNewQueueEntry")
        command_payload = json.loads(msg)
        event = newQueueEntryCreated.from_json(newQueueEntryCreated, command_payload)
        event.publish()

        if command_payload.get("entry_data", {}).get("due_date") is not None:
            gui_recipients = command_payload.get("entry_data").get("assigned_to", [])

            reminder_command = setTaskReminder(owner=command_payload.get("owner"),
                                               reminder_id=str(uuid.uuid4()),
                                               creator=command_payload.get("creator"),
                                               correlation_id=command_payload.get("correlation_id"),
                                               create_time=command_payload.get("create_time"),
                                               task_id=command_payload.get("entry_id"),
                                               reminder_type="gui",
                                               seconds_in_advance=7200,
                                               recipients=gui_recipients
                                               )
            reminder_command.publish()
            reminder_command = setTaskReminder(owner=command_payload.get("owner"),
                                               reminder_id=str(uuid.uuid4()),
                                               creator=command_payload.get("creator"),
                                               correlation_id=command_payload.get("correlation_id"),
                                               create_time=command_payload.get("create_time"),
                                               task_id=command_payload.get("entry_id"),
                                               reminder_type="email",
                                               seconds_in_advance=7200,
                                               recipients=gui_recipients
                                               )
            reminder_command.publish()
            reminder_command = setTaskReminder(owner=command_payload.get("owner"),
                                               reminder_id=str(uuid.uuid4()),
                                               creator=command_payload.get("creator"),
                                               correlation_id=command_payload.get("correlation_id"),
                                               create_time=command_payload.get("create_time"),
                                               task_id=command_payload.get("entry_id"),
                                               reminder_type="gui",
                                               seconds_in_advance=86400,
                                               recipients=gui_recipients
                                               )
            reminder_command.publish()
            reminder_command = setTaskReminder(owner=command_payload.get("owner"),
                                               reminder_id=str(uuid.uuid4()),
                                               creator=command_payload.get("creator"),
                                               correlation_id=command_payload.get("correlation_id"),
                                               create_time=command_payload.get("create_time"),
                                               task_id=command_payload.get("entry_id"),
                                               reminder_type="email",
                                               seconds_in_advance=86400,
                                               recipients=gui_recipients
                                               )
            reminder_command.publish()
        return True

    def newQueueEntryCreated(self, ch, msg):
        try:
            print("Handling newQueueEntryCreated")
            json_data = json.loads(msg)
            new_queue_entry = newQueueEntryCreated.from_json(newQueueEntryCreated, json_data)

            queue_entry = Config.db_session.query(QueueEntry) \
                .filter(QueueEntry.consumer_id == new_queue_entry.owner) \
                .filter(QueueEntry.entry_id == new_queue_entry.entry_id) \
                .first()

            new_queue_entry.entry_data["queue_id"] = new_queue_entry.entry_data.get("queue_id")
            if queue_entry is None:
                queue_entry = QueueEntry()
                queue_entry.consumer_id = new_queue_entry.owner
                queue_entry.owner_id = new_queue_entry.owner

            queue_entry.creator = new_queue_entry.creator
            queue_entry.queue_id = new_queue_entry.entry_data.get("queue_id")
            queue_entry.entry_id = new_queue_entry.entry_id
            queue_entry.order = new_queue_entry.entry_data.get("order") if new_queue_entry.entry_data.get(
                "order") is not None else 0
            queue_entry.data = json.dumps(new_queue_entry.entry_data)
            queue_entry.links = json.dumps(new_queue_entry.links) if new_queue_entry.links is not None else json.dumps(
                [])
            queue_entry.data_owners = json.dumps(
                new_queue_entry.data_owners) if new_queue_entry.data_owners is not None else json.dumps(
                [f"user.{queue_entry.creator}"])
            queue_entry.save()

            message = {"command": "refreshQueueEntries",
                       "args": {"queue_id": queue_entry.queue_id, "queue_entry_id": queue_entry.entry_id,
                                "task_id": json_data.get("correlation_id")}}

            payload = {"room": queue_entry.consumer_id, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))

            message = {"command": "newQueueEntryCreated",
                       "args": {"queue_id": queue_entry.queue_id, "queue_entry_id": queue_entry.entry_id,
                                "data": new_queue_entry.entry_data,
                                "task_id": json_data.get("correlation_id")}}

            payload = {"room": new_queue_entry.creator, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))

            if new_queue_entry.entry_data.get("create_easy2schedule_event", False) is True:
                startdate = dateutil.parser.isoparse(new_queue_entry.entry_data.get("due_date"))
                cet = pytz.timezone('CET')
                startdate = startdate.astimezone(cet)
                command_payload = {
                    "creator_consumer_name": new_queue_entry.owner,
                    "correlation_id": new_queue_entry.correlation_id,
                    "consumer_id": new_queue_entry.owner,
                    "owner": new_queue_entry.owner,
                    "creator": new_queue_entry.creator,
                    "event_id": queue_entry.entry_id,
                    "resourceId": "tasks",
                    "textColor": "",
                    "color": "",
                    "attachedRessources": [
                        "{\"id\":1,\"name\":\"" + new_queue_entry.creator + "\",\"fontColor\":\"#000000\",\"backgroundColor\":\"#dddddd\",\"loginId\":\"" + new_queue_entry.creator + "\"}"
                    ],
                    "starttime": (startdate + timedelta(minutes=-60)).strftime('%Y-%m-%d %H:%M:%S'),
                    "endtime": startdate.strftime('%Y-%m-%d %H:%M:%S'),
                    "title": new_queue_entry.entry_data.get("title"),
                    "savekind": "create",
                    "extended_props": {
                        "creator_login": new_queue_entry.creator
                    },
                    "allDay": False,
                    "data_owners": [],
                    "attachedContacts": [],
                    "editable": False,
                    "reminders": [
                        {"type": "gui", "seconds_in_advance": 3600, "recipients": [new_queue_entry.creator]},
                        {"type": "gui", "seconds_in_advance": 60, "recipients": [new_queue_entry.creator]}
                    ]
                }
                event = newAppointmentCreated.from_json(newAppointmentCreated, command_payload)
                event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def updateQueueEntry(self, ch, msg):
        print("Handling updateQueueEntry")
        command_payload = json.loads(msg)
        event = queueEntryUpdated.from_json(queueEntryUpdated, command_payload)
        event.publish()
        return True

    def queueEntryUpdated(self, ch, msg):
        try:
            print("Handling queueEntryUpdated")
            json_data = json.loads(msg)
            queueentry_updated_event = queueEntryUpdated.from_json(queueEntryUpdated, json_data)

            queue_entry = Config.db_session.query(QueueEntry) \
                .filter(QueueEntry.consumer_id == queueentry_updated_event.owner) \
                .filter(QueueEntry.entry_id == queueentry_updated_event.entry_id) \
                .first()

            if not queue_entry:
                raise Exception("QueueEntry not found")

            requesters_group_memberships = ApiCalls().getGroupMembershipsByCustomerIdAndLogin(json_data.get("owner"),
                                                                                              json_data.get("creator"))

            data_owners = json.loads(queue_entry.data_owners)
            matches = [match for match in requesters_group_memberships if match in data_owners]
            if f"user.{queueentry_updated_event.creator}" in data_owners:
                matches.append(f"user.{queueentry_updated_event.creator}")

            if len(matches) == 0 and len(data_owners) > 0:
                message = {"command": "showTaskError",
                           "args": {"text": "You are not allowed to edit this queue_entry",
                                    "task_id": json_data.get("correlation_id"),
                                    "entry_id": queueentry_updated_event.entry_id}}
                payload = {"room": queue_entry.consumer_id, "data": message}
                client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
                return True

            # queue_entry.order = queueentry_updated_event.entry_data.get("order") if queueentry_updated_event.entry_data.get("order") is not None else queue_entry.order
            queue_entry.data = json.dumps(queueentry_updated_event.entry_data)
            queue_entry.links = json.dumps(
                queueentry_updated_event.links) if queueentry_updated_event.links is not None else json.dumps([])
            event_data_json = queueentry_updated_event.entry_data

            if event_data_json.get("queue_id") is not None:
                queue_entry.queue_id = event_data_json.get("queue_id")
            queue_entry.save()

            reminders = Config.db_session.query(TaskReminder).filter_by(task_id=queueentry_updated_event.entry_id).all()

            for reminder in reminders:
                reminder_command = deleteTaskReminder(owner_id=json_data.get("owner"),
                                                      creator=json_data.get("creator"),
                                                      reminder_id=reminder.reminder_id,
                                                      correlation_id=json_data.get("correlation_id"),
                                                      create_time=json_data.get("create_time"),
                                                      skip_validation=True)
                #reminder_command.publish()

            gui_recipients = json_data.get("entry_data").get("assigned_to", [])
            reminder_command = setTaskReminder(owner=json_data.get("owner"),
                                               reminder_id=str(uuid.uuid4()),
                                               creator=json_data.get("creator"),
                                               correlation_id=json_data.get("correlation_id"),
                                               create_time=json_data.get("create_time"),
                                               task_id=json_data.get("entry_id"),
                                               reminder_type="gui",
                                               seconds_in_advance=7200,
                                               recipients=gui_recipients
                                               )
            #reminder_command.publish()
            reminder_command = setTaskReminder(owner=json_data.get("owner"),
                                               reminder_id=str(uuid.uuid4()),
                                               creator=json_data.get("creator"),
                                               correlation_id=json_data.get("correlation_id"),
                                               create_time=json_data.get("create_time"),
                                               task_id=json_data.get("entry_id"),
                                               reminder_type="email",
                                               seconds_in_advance=7200,
                                               recipients=gui_recipients
                                               )
            #reminder_command.publish()
            reminder_command = setTaskReminder(owner=json_data.get("owner"),
                                               reminder_id=str(uuid.uuid4()),
                                               creator=json_data.get("creator"),
                                               correlation_id=json_data.get("correlation_id"),
                                               create_time=json_data.get("create_time"),
                                               task_id=json_data.get("entry_id"),
                                               reminder_type="gui",
                                               seconds_in_advance=86400,
                                               recipients=gui_recipients
                                               )
            #reminder_command.publish()
            reminder_command = setTaskReminder(owner=json_data.get("owner"),
                                               reminder_id=str(uuid.uuid4()),
                                               creator=json_data.get("creator"),
                                               correlation_id=json_data.get("correlation_id"),
                                               create_time=json_data.get("create_time"),
                                               task_id=json_data.get("entry_id"),
                                               reminder_type="email",
                                               seconds_in_advance=86400,
                                               recipients=gui_recipients
                                               )
            #reminder_command.publish()

            message = {"command": "refreshQueueEntries",
                       "args": {"entry_id": queue_entry.entry_id, "queue_id": queue_entry.queue_id,
                                "task_id": json_data.get("correlation_id")}}

            payload = {"room": queue_entry.consumer_id, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))

            deleteAppointmentCommandPayload = {
                    "consumer_id": queueentry_updated_event.owner,
                    "owner": queueentry_updated_event.owner,
                    "creator": queueentry_updated_event.creator,
                    "creator_consumer_name": queueentry_updated_event.owner,
                    "correlation_id": queueentry_updated_event.correlation_id,
                    "create_time": queueentry_updated_event.create_time,
                    "event_id": queueentry_updated_event.entry_id
                }

            event = appointmentDeleted.from_json(appointmentDeleted, deleteAppointmentCommandPayload)
            event.publish()

            if queueentry_updated_event.entry_data.get("create_easy2schedule_event", False) is True:
                startdate = dateutil.parser.isoparse(queueentry_updated_event.entry_data.get("due_date"))
                cet = pytz.timezone('CET')
                startdate = startdate.astimezone(cet)
                command_payload = {
                    "create_time": queueentry_updated_event.create_time,
                    "creator_consumer_name": queueentry_updated_event.owner,
                    "correlation_id": queueentry_updated_event.correlation_id,
                    "consumer_id": queueentry_updated_event.owner,
                    "owner": queueentry_updated_event.owner,
                    "creator": queueentry_updated_event.creator,
                    "event_id": queueentry_updated_event.entry_id,
                    "resourceId": "tasks",
                    "textColor": "",
                    "color": "",
                    "attachedRessources": [
                        "{\"id\":1,\"name\":\"" + queueentry_updated_event.creator + "\",\"fontColor\":\"#000000\",\"backgroundColor\":\"#dddddd\",\"loginId\":\"" + queueentry_updated_event.creator + "\"}"
                    ],
                    "starttime": (startdate + timedelta(minutes=-60)).strftime('%Y-%m-%d %H:%M:%S'),
                    "endtime": startdate.strftime('%Y-%m-%d %H:%M:%S'),
                    "title": queueentry_updated_event.entry_data.get("title"),
                    "savekind": "create",
                    "extended_props": {
                        "creator_login": queueentry_updated_event.creator
                    },
                    "allDay": False,
                    "data_owners": [],
                    "attachedContacts": [],
                    "editable": False,
                    "reminders": [
                        {"type": "gui", "seconds_in_advance": 3600, "recipients": [queueentry_updated_event.creator]},
                        {"type": "gui", "seconds_in_advance": 60, "recipients": [queueentry_updated_event.creator]}
                    ]
                }
                event = newAppointmentCreated.from_json(newAppointmentCreated, command_payload)
                event.publish()


        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def deleteQueueEntry(self, ch, msg):
        print("Handling deleteQueueEntry")
        command_payload = json.loads(msg)
        event = queueEntryDeleted.from_json(queueEntryDeleted, command_payload)
        event.publish()

        reminders = Config.db_session.query(TaskReminder).filter_by(task_id=command_payload.get("entry_id")).all()

        for reminder in reminders:
            reminder_command = deleteTaskReminder(owner_id=command_payload.get("owner"),
                                                  creator=command_payload.get("creator"),
                                                  reminder_id=reminder.reminder_id,
                                                  correlation_id=command_payload.get("correlation_id"),
                                                  create_time=command_payload.get("create_time"),
                                                  skip_validation=True)
            reminder_command.publish()
        return True

    def queueEntryDeleted(self, ch, msg):
        try:
            print("Handling queueEntryDeleted")
            json_data = json.loads(msg)
            queue_entry_deleted_event = queueEntryDeleted.from_json(queueEntryDeleted, json_data)

            entry = Config.db_session.query(QueueEntry) \
                .filter(QueueEntry.consumer_id == queue_entry_deleted_event.owner) \
                .filter(QueueEntry.entry_id == queue_entry_deleted_event.entry_id) \
                .one_or_none()

            queue_id = "unknown"
            if entry is not None:
                queue_id = entry.queue_id

            Config.db_session.query(QueueEntry) \
                .filter(QueueEntry.consumer_id == queue_entry_deleted_event.owner) \
                .filter(QueueEntry.entry_id == queue_entry_deleted_event.entry_id) \
                .delete()

            message = {"command": "refreshQueueEntries",
                       "args": {"queue_id": queue_id, "queue_entry_id": "deleted",
                                "task_id": json_data.get("correlation_id")}}

            payload = {"room": queue_entry_deleted_event.owner, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def queueRemoveDataOwner(self, ch, msg):
        try:
            print("Handling queueDataOwnerRemoved")
            command_payload = json.loads(msg)
            event = queueDataOwnerRemoved.from_json(queueDataOwnerRemoved, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def queueDataOwnerRemoved(self, ch, msg):
        print("Handling queueDataOwnerRemoved")
        try:
            json_data = json.loads(msg)
            remove_owners = json_data.get("data")
            activeQueue = Config.db_session.query(Queue).filter(Queue.consumer_id == json_data.get("owner")).filter(
                Queue.queue_id == remove_owners.get("queue_id")).one_or_none()

            if activeQueue is not None:
                current_data_owners = activeQueue.data_owners
                if current_data_owners is None:
                    current_data_owners = "[]"

                current_data_owners = json.loads(current_data_owners)
                new_data_owners = [owner for owner in current_data_owners if
                                   owner not in remove_owners.get("owners", [])]

                activeQueue.data_owners = json.dumps(new_data_owners)
                activeQueue.save()

                message = {"command": "refreshQueue",
                           "args": {"queue_id": activeQueue.queue_id, "task_id": json_data.get("correlation_id")}}

                payload = {"room": activeQueue.consumer_id, "data": message}
                client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            sleep(5)
            return False

        return True

    def queueAddDataOwner(self, ch, msg):
        try:
            print("Handling queueAddDataOwner")
            command_payload = json.loads(msg)
            event = queueDataOwnerAdded.from_json(queueDataOwnerAdded, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def queueDataOwnerAdded(self, ch, msg):
        print("Handling queueDataOwnerAdded")
        try:
            json_data = json.loads(msg)
            new_owners = json_data.get("data")
            activeQueue = Config.db_session.query(Queue).filter(Queue.consumer_id == json_data.get("owner")).filter(
                Queue.queue_id == new_owners.get("queue_id")).one_or_none()

            if activeQueue is not None:
                current_data_owners = activeQueue.data_owners
                if current_data_owners is None:
                    current_data_owners = "[]"

                current_data_owners = json.loads(current_data_owners)
                merged_owners = current_data_owners + [owner for owner in new_owners.get("owners", []) if
                                                       owner not in current_data_owners]

                activeQueue.data_owners = json.dumps(merged_owners)
                activeQueue.save()

                message = {"command": "refreshQueue",
                           "args": {"queue_id": activeQueue.queue_id, "data_owners": activeQueue.data_owners,
                                    "task_id": json_data.get("correlation_id")}}

                payload = {"room": activeQueue.consumer_id, "data": message}
                client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            sleep(5)
            return False

        return True

    def queueEntryAddDataOwner(self, ch, msg):
        try:
            print("Handling queueEntryAddDataOwner")
            command_payload = json.loads(msg)
            event = queueEntryDataOwnerAdded.from_json(queueEntryDataOwnerAdded, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def queueEntryDataOwnerAdded(self, ch, msg):
        print("Handling queueEntryDataOwnerAdded")
        try:
            json_data = json.loads(msg)
            new_owners = json_data.get("data")
            activeQueueEntry = Config.db_session.query(QueueEntry) \
                .filter(QueueEntry.consumer_id == json_data.get("owner")) \
                .filter(QueueEntry.entry_id == new_owners.get("entry_id")) \
                .one_or_none()

            if activeQueueEntry is not None:
                current_data_owners = activeQueueEntry.data_owners
                if current_data_owners is None:
                    current_data_owners = "[]"

                current_data_owners = json.loads(current_data_owners)
                merged_owners = current_data_owners + [owner for owner in new_owners.get("owners", []) if
                                                       owner not in current_data_owners]

                activeQueueEntry.data_owners = json.dumps(merged_owners)
                activeQueueEntry.save()

                message = {"command": "refreshQueueEntry",
                           "args": {"entry_id": activeQueueEntry.entry_id, "queue_id": activeQueueEntry.queue_id,
                                    "data_owners": activeQueueEntry.data_owners,
                                    "task_id": json_data.get("correlation_id")}}

                payload = {"room": activeQueueEntry.consumer_id, "data": message}
                client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            sleep(5)
            return False

        return True

    def queueEntryRemoveDataOwner(self, ch, msg):
        try:
            print("Handling queueEntryRemoveDataOwner")
            command_payload = json.loads(msg)
            event = queueEntryDataOwnerRemoved.from_json(queueEntryDataOwnerRemoved, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def queueEntryDataOwnerRemoved(self, ch, msg):
        print("Handling queueEntryDataOwnerRemoved")
        try:
            json_data = json.loads(msg)
            remove_owners = json_data.get("data")
            activeQueueEntry = Config.db_session.query(QueueEntry).filter(
                QueueEntry.consumer_id == json_data.get("owner")).filter(
                QueueEntry.entry_id == remove_owners.get("entry_id")).one_or_none()

            if activeQueueEntry is not None:
                current_data_owners = activeQueueEntry.data_owners
                if current_data_owners is None:
                    current_data_owners = "[]"

                current_data_owners = json.loads(current_data_owners)
                new_data_owners = [owner for owner in current_data_owners if
                                   owner not in remove_owners.get("owners", [])]

                activeQueueEntry.data_owners = json.dumps(new_data_owners)
                activeQueueEntry.save()

                message = {"command": "refreshQueueEntry",
                           "args": {"entry_id": activeQueueEntry.entry_id, "queue_id": activeQueueEntry.queue_id,
                                    "data_owners": activeQueueEntry.data_owners,
                                    "task_id": json_data.get("correlation_id")}}

                payload = {"room": activeQueueEntry.consumer_id, "data": message}
                client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            sleep(5)
            return False

        return True

    def createNewTaskTemplate(self, ch, msg):
        print("Handling createNewTaskTemplate")
        command_payload = json.loads(msg)
        event = newTaskTemplateCreated.from_json(newTaskTemplateCreated, command_payload)
        event.publish()
        return True

    def newTaskTemplateCreated(self, ch, msg):
        try:
            print("Handling newTaskTemplateCreated")
            json_data = json.loads(msg)
            new_template = newTaskTemplateCreated.from_json(newTaskTemplateCreated, json_data)

            template = Config.db_session.query(TaskTemplate) \
                .filter(TaskTemplate.consumer_id == new_template.owner) \
                .filter(TaskTemplate.template_id == new_template.template_id) \
                .first()

            new_template.template_data["queue_id"] = new_template.template_data.get("queue_id")
            if template is None:
                template = TaskTemplate()
                template.consumer_id = new_template.owner
                template.owner_id = new_template.owner

            template.creator = new_template.creator
            template.queue_id = new_template.template_data.get("queue_id")
            template.template_id = new_template.template_id
            template.data = json.dumps(new_template.template_data)
            template.data_owners = json.dumps(
                new_template.data_owners) if new_template.data_owners is not None else json.dumps(
                [f"user.{template.creator}"])
            template.save()

            message = {"command": "refreshTemplates",
                       "args": {"queue_id": template.queue_id, "task_template_id": template.template_id,
                                "task_id": json_data.get("correlation_id")}}

            payload = {"room": template.consumer_id, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def deleteTaskTemplate(self, ch, msg):
        print("Handling deleteTaskTemplate")
        command_payload = json.loads(msg)
        event = taskTemplateDeleted.from_json(taskTemplateDeleted, command_payload)
        event.publish()
        return True

    def taskTemplateDeleted(self, ch, msg):
        try:
            print("Handling taskTemplateDeleted")
            json_data = json.loads(msg)
            template_deleted_event = taskTemplateDeleted.from_json(taskTemplateDeleted, json_data)

            template = Config.db_session.query(TaskTemplate) \
                .filter(TaskTemplate.consumer_id == template_deleted_event.owner) \
                .filter(TaskTemplate.template_id == template_deleted_event.template_id) \
                .one_or_none()

            queue_id = "unknown"
            if template is not None:
                queue_id = template.queue_id

            Config.db_session.query(TaskTemplate) \
                .filter(TaskTemplate.consumer_id == template_deleted_event.owner) \
                .filter(TaskTemplate.template_id == template_deleted_event.template_id) \
                .delete()

            message = {"command": "refreshTemplates",
                       "args": {"queue_id": queue_id, "task_template_id": "deleted",
                                "task_id": json_data.get("correlation_id")}}

            payload = {"room": template_deleted_event.owner, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def taskReminderSet(self, channel, msg):
        try:
            print("Handling taskReminderSet")
            msg = json.loads(msg)
            event_reminder = Config.db_session.query(TaskReminder) \
                .filter(TaskReminder.reminder_id == msg.get("reminder_id")) \
                .filter(TaskReminder.owner == msg.get("owner")) \
                .filter(TaskReminder.creator == msg.get("creator")) \
                .filter(TaskReminder.task_id == msg.get("task_id")).one_or_none()

            if event_reminder is None:
                event_reminder = TaskReminder()
                event_reminder.reminder_id = msg.get("reminder_id")
                event_reminder.owner = msg.get("owner")
                event_reminder.creator = msg.get("creator")
                event_reminder.task_id = msg.get("task_id")
                event_reminder.creation_time = msg.get("creation_time")

            event_reminder.data_owners = json.dumps([msg.get("creator")])
            event_reminder.seconds_in_advance = msg.get("seconds_in_advance")
            event_reminder.definition = json.dumps(msg)
            event_reminder.processed_time = None
            event_reminder.save()
        except Exception as e:
            print(e)
            traceback.print_exc()
            return False
        return True

    def setTaskReminder(self, channel, msg):
        try:
            print("Handling setTaskReminder")
            command_payload = json.loads(msg)
            event = taskReminderSet.from_json(taskReminderSet, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def taskReminderDeleted(self, channel, msg):
        try:
            print("Handling taskReminderDeleted")
            command_payload = json.loads(msg)
            Config.db_session.query(TaskReminder) \
                .filter(TaskReminder.owner == command_payload.get("owner")) \
                .filter(TaskReminder.reminder_id == command_payload.get("reminder_id")) \
                .delete()
            Config.db_session.flush()

            message = {"event": "taskReminderDeleted",
                       "args": {"correlation_id": command_payload.get("correlation_id"),
                                "reminder_id": command_payload.get("reminder_id")}}

            payload = {"room": command_payload.get("owner"), "data": message}
            channel.basic_publish(exchange="socketserver", routing_key="notifications", body=json.dumps(payload))

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False
        return True

    def deleteTaskReminder(self, channel, msg):
        try:
            print("Handling deleteTaskReminder")
            command_payload = json.loads(msg)
            event = taskReminderDeleted.from_json(taskReminderDeleted, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def taskReminderSent(self, channel, msg):
        try:
            print("Handling taskReminderSent", msg)
            message_payload = json.loads(msg)
            print("MESSAGE_PAYLOAD", message_payload)
            task_reminder = Config.db_session.query(TaskReminder).filter(
                TaskReminder.reminder_id == message_payload.get("reminder_id")).one_or_none()
            event_payload = json.loads(task_reminder.definition)
            print("task_payload", event_payload)
            print("task", task_reminder)

        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            if "No row was found" in str(e):
                return True
            return False
        return True

    def sendTaskReminder(self, channel, msg):
        result = False
        try:
            print("Handling sendTaskReminder", msg)

            command_payload = json.loads(msg)
            result = Reminder(command_payload.get("reminder_id"), self.queue_client).sendReminder()

            event = taskReminderSent.from_json(taskReminderSent, command_payload)
            event.publish()
            return result

        except Exception as e:
            print(e)
            traceback.print_exc()
            sleep(5)
            return False

    def deleteTaskManagerResource(self, ch, msg):
        print("Handling deleteTaskManagerResource")
        command_payload = json.loads(msg)
        event = taskManagerResourceDeleted.from_json(taskManagerResourceDeleted, command_payload)
        event.publish()
        return True

    def taskManagerResourceDeleted(self, ch, msg):
        try:
            print("Handling taskManagerResourceDeleted")
            json_data = json.loads(msg)
            resource_deleted_event = taskManagerResourceDeleted.from_json(taskManagerResourceDeleted, json_data)

            resource = Config.db_session.query(Resource) \
                .filter(Resource.consumer_id == resource_deleted_event.owner) \
                .filter(Resource.resource_id == resource_deleted_event.resource_id) \
                .one_or_none()

            Config.db_session.query(Resource) \
                .filter(Resource.consumer_id == resource_deleted_event.owner) \
                .filter(Resource.resource_id == resource_deleted_event.resource_id) \
                .delete()

            message = {"command": "refreshTaskManagerResources",
                       "args": {"taskmanager_resource_id": "deleted", "task_id": json_data.get("correlation_id")}}

            payload = {"room": resource_deleted_event.owner, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def createNewTaskManagerResource(self, ch, msg):
        print("Handling createNewTaskManagerResource")
        command_payload = json.loads(msg)
        event = newTaskManagerResourceCreated.from_json(newTaskManagerResourceCreated, command_payload)
        event.publish()
        return True

    def newTaskManagerResourceCreated(self, ch, msg):
        try:
            print("Handling newTaskManagerResourceCreated")
            json_data = json.loads(msg)
            new_resource = newTaskManagerResourceCreated.from_json(newTaskManagerResourceCreated, json_data)

            resource = Config.db_session.query(Resource) \
                .filter(Resource.consumer_id == new_resource.owner) \
                .filter(Resource.resource_id == new_resource.resource_id) \
                .first()

            if resource is None:
                resource = Resource()
                resource.consumer_id = new_resource.owner
                resource.owner_id = new_resource.owner

            resource.creator = new_resource.creator
            resource.type = new_resource.type
            resource.resource_id = new_resource.resource_id
            resource.data = json.dumps(new_resource.resource_data)
            resource.data_owners = json.dumps(
                new_resource.data_owners) if new_resource.data_owners is not None else json.dumps(
                [f"user.{template.creator}"])
            resource.save()

            message = {"command": "refreshTaskManagerResources",
                       "args": {"taskmanager_resource_id": resource.resource_id,
                                "task_id": json_data.get("correlation_id")}}

            payload = {"room": resource.consumer_id, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def updateTaskManagerResource(self, channel, msg):
        try:
            print("Handling updateTaskManagerResource")
            command_payload = json.loads(msg)
            event = taskManagerResourceUpdated.from_json(taskManagerResourceUpdated, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def taskManagerResourceUpdated(self, channel, msg):
        try:
            payload_json = json.loads(msg)

            resource = Config.db_session.query(Resource) \
                .filter(Resource.consumer_id == payload_json.get("owner")) \
                .filter(Resource.resource_id == payload_json.get("resource_id")) \
                .one_or_none()

            if resource is None:
                message = {"event": "taskManagerResourceUpdatedFailed",
                           "args": {"correlation_id": payload_json.get("correlation_id"),
                                    "error_message": "Ressource not found"}}
                payload = {"room": payload_json.get("creator"), "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                          message=json.dumps(payload))
                return True

            resource.type = payload_json.get("type", "not set")
            resource.data = json.dumps(payload_json.get("resource_data", "{}"))
            resource.save()

            resource_event = {"id": resource.id,
                              "type": payload_json.get("type"),
                              "resource_data": payload_json.get("resource_data"),
                              "resource_id": resource.resource_id
                              }

            message = {"command": "refreshTaskManagerResources",
                       "args": {"taskmanager_resource_id": resource.resource_id,
                                "task_id": payload_json.get("correlation_id")}}

            payload = {"room": payload_json.get("owner"), "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            return False
        return True

    def createNewTaskManagerTemplateGroup(self, ch, msg):
        print("Handling createNewTaskManagerResource")
        command_payload = json.loads(msg)
        event = newTaskManagerTemplateGroupCreated.from_json(newTaskManagerTemplateGroupCreated, command_payload)
        event.publish()
        return True

    def newTaskManagerTemplateGroupCreated(self, ch, msg):
        try:
            print("Handling newTaskManagerTemplateGroupCreated")
            json_data = json.loads(msg)
            new_template_group = newTaskManagerTemplateGroupCreated.from_json(newTaskManagerTemplateGroupCreated,
                                                                              json_data)

            template_group = Config.db_session.query(TemplateGroup) \
                .filter(TemplateGroup.consumer_id == new_template_group.owner) \
                .filter(TemplateGroup.group_id == new_template_group.group_id) \
                .first()

            if template_group is None:
                template_group = TemplateGroup()
                template_group.consumer_id = new_template_group.owner
                template_group.owner_id = new_template_group.owner

            template_group.creator = new_template_group.creator

            template_group.scheduling_interval = int(
                new_template_group.scheduling_interval) if new_template_group.scheduling_interval is not None else None
            template_group.start_date = datetime.strptime(new_template_group.start_date,
                                                          '%Y-%m-%d %H:%M:%S') if new_template_group.start_date is not None else None
            template_group.group_id = new_template_group.group_id
            template_group.data = json.dumps(new_template_group.template_group_data)
            template_group.data_owners = json.dumps(
                new_template_group.data_owners) if new_template_group.data_owners is not None else json.dumps(
                [f"user.{template.creator}"])
            template_group.save()

            message = {"command": "refreshTaskManagerTemplateGroups",
                       "args": {"taskmanager_template_group_id": template_group.group_id,
                                "task_id": json_data.get("correlation_id")}}

            payload = {"room": template_group.consumer_id, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def updateTaskManagerTemplateGroup(self, channel, msg):
        try:
            print("Handling updateTaskManagerTemplateGroup")
            command_payload = json.loads(msg)
            event = taskManagerTemplateGroupUpdated.from_json(taskManagerTemplateGroupUpdated, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def taskManagerTemplateGroupUpdated(self, channel, msg):
        try:
            payload_json = json.loads(msg)

            template_group = Config.db_session.query(TemplateGroup) \
                .filter(TemplateGroup.consumer_id == payload_json.get("owner")) \
                .filter(TemplateGroup.group_id == payload_json.get("group_id")) \
                .one_or_none()

            if template_group is None:
                message = {"event": "taskManagerTemplateGroupUpdatedFailed",
                           "args": {"correlation_id": payload_json.get("correlation_id"),
                                    "error_message": "Ressource not found"}}
                payload = {"room": payload_json.get("creator"), "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                          message=json.dumps(payload))
                return True

            new_start_date = datetime.strptime(payload_json.get("start_date"), '%Y-%m-%d %H:%M:%S') if payload_json.get(
                "start_date") is not None else None
            if template_group.start_date != new_start_date:
                template_group.run_counter = 0

            template_group.scheduling_interval = int(payload_json.get("scheduling_interval", "")) if payload_json.get(
                "scheduling_interval") is not None else None
            template_group.start_date = new_start_date
            template_group.data = json.dumps(payload_json.get("template_group_data", "{}"))
            template_group.save()

            template_group = {"id": template_group.id,
                              "scheduling_interval": payload_json.get("scheduling_interval"),
                              "group_data": payload_json.get("template_group_data"),
                              }

            message = {"command": "refreshTaskManagerTemplateGroups",
                       "args": {"taskmanager_template_group_id": template_group,
                                "task_id": payload_json.get("correlation_id")}}
            payload = {"room": payload_json.get("owner"), "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            return False

        return True

    def deleteTaskManagerTemplateGroup(self, ch, msg):
        print("Handling deleteTaskManagerTemplateGroup")
        command_payload = json.loads(msg)
        event = taskManagerTemplateGroupDeleted.from_json(taskManagerTemplateGroupDeleted, command_payload)
        event.publish()
        return True

    def taskManagerTemplateGroupDeleted(self, ch, msg):
        try:
            print("Handling taskManagerTemplateGroupDeleted")
            json_data = json.loads(msg)
            template_group_deleted_event = taskManagerTemplateGroupDeleted.from_json(taskManagerTemplateGroupDeleted,
                                                                                     json_data)

            template_group = Config.db_session.query(TemplateGroup) \
                .filter(TemplateGroup.consumer_id == template_group_deleted_event.owner) \
                .filter(TemplateGroup.group_id == template_group_deleted_event.group_id) \
                .one_or_none()

            Config.db_session.query(TemplateGroup) \
                .filter(TemplateGroup.consumer_id == template_group_deleted_event.owner) \
                .filter(TemplateGroup.group_id == template_group_deleted_event.group_id) \
                .delete()

            message = {"command": "refreshTaskManagerTemplateGroups",
                       "args": {"taskmanager_template_group_id": "deleted", "task_id": json_data.get("correlation_id")}}

            payload = {"room": template_group_deleted_event.owner, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True
