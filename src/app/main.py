from flask import Flask, jsonify, request, Response, stream_with_context
import json
from collections import OrderedDict

from pbglobal.pblib.api_calls import ApiCalls
from config import Config
from db.queue import Queue
from db.queue_entry import QueueEntry
from db.queue_entry import AlchemyEncoder
from db.task_template import TaskTemplate
from db.resource import Resource
from db.template_group import TemplateGroup
from pbglobal.pblib.api_calls import ApiCalls
import datetime
import pytz
from pbglobal.commands.createNewQueueEntry import createNewQueueEntry
from pbglobal.pblib.amqp import client

from pbglobal.pblib.pbdatecalc import PbDateCalc

app = Flask(__name__)

@app.route('/queues/list', methods=["GET"])
def contacts_list():
    app.config['JSON_SORT_KEYS'] = True
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    apikey = request.headers.get("apikey", "UNKNOWN")
    results = Config.db_session.query(Queue) \
        .filter(Queue.consumer_id == consumer_id) \
        .all()

    login = ApiCalls().getLoginByApiKey(apikey)
    requesters_group_memberships = ApiCalls().getGroupMembershipsByApikey(apikey)

    return_value = {}
    for result in results:
        data_owners = json.loads(result.data_owners)
        matches = [match for match in requesters_group_memberships if match in data_owners]
        if f"user.{login}" in data_owners:
            matches.append(f"user.{login}")

        if len(matches) == 0 and len(data_owners) > 0:
            continue

        return_value[result.queue_id] = {"queue_data": json.loads(result.data), "owners": data_owners}
    return jsonify({"queues": return_value})

@app.route('/get_queue_entries/<queue_id>', methods=["GET"])
def get_queue_entries(queue_id=None):
    app.config['JSON_SORT_KEYS'] = False
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    apikey = request.headers.get("apikey", "UNKNOWN")
    results = Config.db_session.query(QueueEntry) \
        .filter(QueueEntry.consumer_id == consumer_id) \
        .filter(QueueEntry.queue_id == queue_id) \
        .order_by(QueueEntry.order.asc())\
        .all()

    login = ApiCalls().getLoginByApiKey(apikey)
    requesters_group_memberships = ApiCalls().getGroupMembershipsByApikey(apikey)

    return_value = {}

    for result in results:
        data_owners = json.loads(result.data_owners)
        matches = [match for match in requesters_group_memberships if match in data_owners]
        if f"user.{login}" in data_owners:
            matches.append(f"user.{login}")

        if len(matches) == 0 and len(data_owners) > 0:
            continue


        entry_data = json.loads(result.data)
        if entry_data.get("queue_id") is None:
            entry_data["queue_id"] = queue_id

        entry_data["create_time"] = result.create_time.strftime('%Y-%m-%d %H:%M:%S')

        return_value[result.entry_id] = {"entry_data": entry_data, "owners": data_owners, "links": json.loads(result.links)}

    return jsonify({"queue_entries": return_value})

@app.route('/tasks/by_id/<task_id>', methods=["GET"])
def get_task_by_id(task_id=None):
    app.config['JSON_SORT_KEYS'] = False
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    apikey = request.headers.get("apikey", "UNKNOWN")
    result = Config.db_session.query(QueueEntry) \
        .filter(QueueEntry.consumer_id == consumer_id) \
        .filter(QueueEntry.entry_id == task_id) \
        .order_by(QueueEntry.order.asc())\
        .first()

    login = ApiCalls().getLoginByApiKey(apikey)
    requesters_group_memberships = ApiCalls().getGroupMembershipsByApikey(apikey)

    if result is None:
        return jsonify({"error": "No task with this id"}), 404
    data_owners = json.loads(result.data_owners)
    matches = [match for match in requesters_group_memberships if match in data_owners]
    if f"user.{login}" in data_owners:
        matches.append(f"user.{login}")

    result = json.loads(result.data)
    if len(matches) == 0 and len(data_owners) > 0:
        return jsonify({})
    else:
        return jsonify({"task_data": result})

@app.route('/task_templates/list_by_queue/<queue_id>', methods=["GET"])
def get_task_templates(queue_id=None):
    app.config['JSON_SORT_KEYS'] = False
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    apikey = request.headers.get("apikey", "UNKNOWN")
    results = Config.db_session.query(TaskTemplate) \
        .filter(TaskTemplate.consumer_id == consumer_id) \
        .filter(TaskTemplate.queue_id == queue_id) \
        .all()

    login = ApiCalls().getLoginByApiKey(apikey)
    requesters_group_memberships = ApiCalls().getGroupMembershipsByApikey(apikey)

    return_value = {}

    for result in results:
        data_owners = json.loads(result.data_owners)
        matches = [match for match in requesters_group_memberships if match in data_owners]
        if f"user.{login}" in data_owners:
            matches.append(f"user.{login}")

        if len(matches) == 0 and len(data_owners) > 0:
            continue


        template_data = json.loads(result.data)
        if template_data.get("queue_id") is None:
            template_data["queue_id"] = queue_id

        template_data["create_time"] = result.create_time.strftime('%Y-%m-%d %H:%M:%S')

        return_value[result.template_id] = {"entry_data": template_data, "owners": data_owners}

    return jsonify({"templates": return_value})

@app.route('/task_template/by_id/<template_id>', methods=["GET"])
def get_task_template_by_id(template_id=None):
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    apikey = request.headers.get("apikey", "UNKNOWN")
    result = Config.db_session.query(TaskTemplate) \
        .filter(TaskTemplate.consumer_id == consumer_id) \
        .filter(TaskTemplate.template_id == template_id) \
        .first()

    if result:
        template_data = json.loads(result.data)
        return jsonify({"data": template_data})

    return jsonify({"error": "template not found"}), 404

@app.route('/get_taskmanager_resources', methods=["GET"])
def get_taskmanager_resources():
    app.config['JSON_SORT_KEYS'] = False
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    resources = []

    for instance in Config.db_session.query(Resource).filter_by(owner_id=consumer_id):
        resource_data = instance.data
        resource_data = json.loads(resource_data)
        taskmanager_resource = {"resource_id": instance.resource_id,
                              "type": instance.type,
                              "data": json.loads(instance.data)
                              }

        resources.append(taskmanager_resource)
    return jsonify(resources), 200

@app.route('/get_taskmanager_template_groups', methods=["GET"])
def get_taskmanager_template_groups():
    app.config['JSON_SORT_KEYS'] = False
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    template_groups = []

    for instance in Config.db_session.query(TemplateGroup).filter_by(owner_id=consumer_id):
        template_group_data = instance.data
        template_group_data = json.loads(template_group_data)
        taskmanager_template_group = {"group_id": instance.group_id,
                                      "start_date": instance.start_date,
                                      "scheduling_interval": instance.scheduling_interval,
                                      "last_run": instance.last_run,
                                      "data": json.loads(instance.data)
                                     }

        template_groups.append(taskmanager_template_group)
    return jsonify(template_groups), 200

@app.route('/create_tasks_of_template_group/<template_group_id>', methods=["GET"])
def create_tasks_from_template(template_group_id):
    app.config['JSON_SORT_KEYS'] = False
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    apikey = request.headers.get("apikey", "UNKNOWN")
    login = ApiCalls().getLoginByApiKey(apikey)
    current_timezone = pytz.timezone("Europe/Berlin")
    current_time = datetime.datetime.now(pytz.timezone('Europe/Berlin'))

    sql = "SELECT * FROM template_groups where consumer_id=:consumer_id and group_id = :template_group_id"
    results = Config.db_session.execute(sql, {"consumer_id": consumer_id, "template_group_id": template_group_id}).fetchall()
    scheduled_template_ids = {}
    to_be_scheduled_template_list = None
    for row in results:
        to_be_scheduled_template_list = json.loads(row.data).get("templates")
        for template in to_be_scheduled_template_list:
            for template_entry in template.get("template_ids", ""):
                template_id = template_entry.get("id")
                command_creation_time = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

                sql = "SELECT * FROM task_templates where template_id = :template_id"
                task_template_db = Config.db_session.execute(sql, {"template_id": template_id}).first()

                if task_template_db is None:
                    continue

                scheduled_template_ids[template_id] = json.loads(task_template_db.data)

                sent_command = createNewQueueEntry(skip_validation=True)

                sent_command.owner = task_template_db.owner_id
                sent_command.creator = task_template_db.creator

                sent_command.correlation_id = f"scheduled_by_{login}_{row.group_id}_{template_id}_{command_creation_time}"
                sent_command.create_time = command_creation_time
                sent_command.owner_id = task_template_db.owner_id

                template_data = json.loads(task_template_db.data)
                sent_command.links = template_data.get("links", [])
                del(template_data["links"])
                data = template_data
                data["generatedBy"] = f"Manual_scheduler_triggered_by_{login}_{row.group_id}_{template_id}_{command_creation_time}"

                if type(data["due_date"]) == dict:
                    data["due_date"] = PbDateCalc(data["due_date"]).get_calculated_date().strftime('%Y-%m-%dT%H:%M:%S.000Z')

                sent_command.entry_data = data
                sent_command.data_owners = json.loads(task_template_db.data_owners)

                if sent_command.validate():
                    sent_command.amqp_client = client
                    sent_command.publish()
                else:
                    print(
                        f"ERROR in scheduled task creation for template_group {template_id} in template-group: {row.group_id}")

    return jsonify({"result": "success", "consumer_id": consumer_id, "template_group_id": template_group_id, "to_be_scheduled_template_list": to_be_scheduled_template_list}), 200


@app.route('/healthz', methods=["GET"])
def health_action():
    return jsonify({"message": "OK"}), 200


@app.before_request
def before_request():
    payload = request.get_json(silent=True)
    if payload is not None:
        payload["correlation_id"] = request.headers.get("correlation_id", "")
        request.data = json.dumps(payload)
