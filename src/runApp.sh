#!/bin/sh

echo "Starting crond..."
crontab -l

crond -f -l 8 &
python /src/app/server.py
