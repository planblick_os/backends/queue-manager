FROM registry.gitlab.com/crowdsoft-foundation/various/python-base-image:production

COPY src/requirements.txt /src/app/requirements.txt
RUN pip install -r /src/app/requirements.txt

COPY src /src
COPY  resources/crontab /etc/crontabs/root


EXPOSE 8000
RUN chmod 777 /src/tasks/ -R && chmod +x /src/tasks/ -R
RUN chmod 777 /src/runApp.sh && chmod +x /src/runApp.sh && chmod 777 /src/testApp.sh && chmod +x /src/testApp.sh
WORKDIR "/src/app"
ENTRYPOINT ["/src/runApp.sh", "-r"]
